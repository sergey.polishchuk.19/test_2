const { src, dest } = require("gulp");
const { bs } = require("./serv.js");
const minify = require('gulp-minify');
const uglify = require('gulp-uglify');
const concat = require("gulp-concat")


const scriptsFn = () => {
   src("./src/js/*.js")
   .pipe(uglify())
   .pipe(concat('scripts.min.js'))
   .pipe(minify())
      .pipe(dest("./dist/scripts"))
      .pipe(bs.reload({ stream: true }));
};

exports.scripts = scriptsFn;
