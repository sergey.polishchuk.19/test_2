const { src, dest } = require("gulp");
const sass = require("gulp-sass")(require("sass"));
const { bs } = require("./serv.js");
const sourcemaps = require("gulp-sourcemaps");
const cleanCSS = require('gulp-clean-css');
const concat = require('gulp-concat')
const autoprefixer = require('gulp-autoprefixer');
const csso = require('gulp-csso')


function buildStyles() {
   src("./src/scss/**/*.scss")
      .pipe(sourcemaps.init())
      .pipe(sass().on("error", sass.logError))
      .pipe(autoprefixer({
               cascade: false
            }))
      .pipe(sourcemaps.write())
      .pipe(cleanCSS())
      .pipe(concat('styles.min.css'))
      .pipe(csso())
      .pipe(dest("./dist/css"))
      .pipe(bs.reload({ stream: true }));
}

exports.styles = buildStyles;
